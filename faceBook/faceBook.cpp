#include "faceBook.h"
/*
Realization of all functions that manage the object "Facebook"
*/

FaceBook::FaceBook() :numOfUseres(0),UserPSize(1)
{
	users = new Users*[UserPSize];
	
}

FaceBook::~FaceBook()
{
	int  i;
	for (i = 0; i < numOfUseres; i++)
	{
		delete this->users[i];
	}
	delete[] this->users;
}

Users* FaceBook::getUser(char* nameToSearch)const
{
	/*finding user by name else returnung Null*/
	int i;
	for (i = 0; i < numOfUseres; i++)
	{
		if (strcmp(this->users[i]->getName(), nameToSearch) == 0)
			return this->users[i];
	}
	cout << "the name doesn't exist\n";
	return nullptr;
}

void FaceBook::addMember(char* name, Date& date)
{
	/*adding user to FaceBook and use(if needed) "my realloc" function we built*/
	Users* MemberToAdd = new Member(name,date);
	
	if (UserPSize == numOfUseres)
	{
		UserPSize *= 2;
		users = (Users**)myRealloc((void**)users, UserPSize,numOfUseres);
	}
	users[numOfUseres++] = MemberToAdd;
}

void FaceBook::printMembers()const
{
	int i, counter = 0;
	cout << "The users are:\n";
	for (i = 0; i < numOfUseres; i++)
	{
		if (typeid(*(users[i])) == typeid(Member))
			cout << "user #" << ++counter << ": "<< users[i]->getName()<<endl;
	}
	cout << "---------------------------\n";
	/*int i;
	cout << "friends of " << this->name << ":\n";
	for (i = 0; i < numOfFriends; i++)
	{
		cout << "friend #" << i + 1 << ": " << this->friends[i]->name << " \n";
	}
	cout << "---------------------------\n";*/
}

void FaceBook::connectBetween2Users(Member* m1, Member* m2)
{

	*m1 += *m2;
}

void FaceBook::connectBetweenUserToFanPage(Member* m1, FanPage* fanPage1)
{/* fan page1 adding to users array the wanted user and 
 user1 adding to fan pages array the wanted fan page*/


	*m1 += *fanPage1;
	
}

void FaceBook::addStatusToUser(Users* u1, Status* status)
{/*adding status to posts array in user*/
	u1->addStatus(status);
	if(typeid(*(u1))==typeid(Member))
		((Member*)u1)->updateStatusInAllFriends(status);
}

void FaceBook::printStatusesOfSpecificUser(Users* user)const
{
	user->printStatuses();
}

void FaceBook::addFansPage(char* pageName)
{/*adding fans page to FaceBook's fans pages array and use(if needed) "my realloc" function we built*/
	Users* pageToAdd = new FanPage(pageName);
	

	if (UserPSize == numOfUseres)
	{
		UserPSize *= 2;
		users = (Users**)myRealloc((void**)users, UserPSize,numOfUseres);
	}
	users[numOfUseres++] = pageToAdd;
}

void FaceBook::printFansPages()const
{
	/*print the all fun pages in face book*/
	int i,counter=0;
	cout << "The fans pages are:\n";
	for (i = 0; i < numOfUseres; i++)
	{
		if(typeid(*(users[i]))==typeid(FanPage))
			cout << "fans page #" << ++counter << ": " << users[i]->getName() << endl;
	}
	cout << "---------------------------\n";
	
}
