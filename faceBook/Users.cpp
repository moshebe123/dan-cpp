#include "Users.h"

Users::Users(char* userName) : numOfFriends(0), numOfStatuses(0), numOfFriendsPsize(1), statusesPSize(1)
{
	name = _strdup(userName);
}

Users::~Users()
{
	delete[] this->name;
	if(numOfFriends>0)
		delete[] this->members;
	if (numOfStatuses > 0)
	{
		for (int i = 0; i < numOfStatuses; i++)
		{
			delete this->posts[i];
		}
		delete[] posts;
	}
}

char * Users::getName()
{
	return this->name;
}

void Users::printUser() const
{/*printing user details not including birth date*/
	cout << this->name;
}

void Users::printMembers() const
{
	int i;
	for (i = 0; i < numOfFriends; i++)
	{
		cout << ((Users*)members[i])->name<< endl;
	}
}

void Users::printStatuses() const
{
	int i;
	for (i = 0; i < numOfStatuses; i++)
	{
		posts[i]->show();
		cout << endl;
	}
}

void Users::addStatus(Status * statusToAdd)
{
	/*adding status to the status list (arr) of user*/
	if (numOfStatuses == 0)
		posts = new Status*[statusesPSize];
	if (numOfStatuses == statusesPSize)
	{
		statusesPSize *= 2;
		this->posts = (Status**)myRealloc((void**)this->posts, statusesPSize, numOfStatuses);
	}

	posts[numOfStatuses++] = statusToAdd;
}

bool Users::operator>(const Users & other) const
{
	return this->numOfFriends > other.numOfFriends;
}

void Users::addFriend(Member* friendToAdd)
{
	/*
	 func that adding new user to user's list of member in facebook
	 */
	if (numOfFriends == 0)
		members = new Member*[numOfFriendsPsize];
	if (numOfFriends == numOfFriendsPsize)
	{
		numOfFriendsPsize *= 2;
		members = (Member**)myRealloc((void**)members, numOfFriendsPsize, numOfFriends);
	}

	members[numOfFriends] = friendToAdd;
	numOfFriends++;

}
