#ifndef MEMBER_H
#define MEMBER_H
#include"Users.h"
#include"FansPage.h"


class Member: public Users
{
private:
	int numOfFriendsPosts;
	int numOfPages,pagesPsize;
	Date birthDate;
	FanPage** pages;
	Status* lastTenFriendsPosts[ARR_SIZE_OF_LAST_FRIENDS_POSTS];
	Member(const Member& other) = default;
public:
	Member(char* userName, Date& Bdate) :Users(userName), birthDate(Bdate),
		numOfFriendsPosts(0), numOfPages(0), pagesPsize(1) {};
	virtual ~Member();
	void printLastTenStatusesOfFriends();
	void printUser() const;
	void addFansPage(FanPage* pageToAdd);
	void updateLastTenPostsAfterAddingFriend(Member * newFriend);
	void printStatuses() const;
	void updateLastTenFriendsPosts(Status* status);
	void updateStatusInAllFriends(Status* status);
	 Member& operator+=( Member& other);
	 Member& operator+=( FanPage& other);
	
};


#endif // !MEMBER_H

