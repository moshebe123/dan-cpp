#ifndef FACEBOOK_H
#define FACEBOOK_H

#include "Member.h"
#include "FansPage.h"

/*this object is the main object that manage the all users and fans pages were created*/
class FaceBook
{
private:
	int numOfUseres,UserPSize;
	FaceBook(const FaceBook& other) = default;
	 Users** users;
public:
	FaceBook() ;
	~FaceBook();
	void addMember(char* name,Date& date);
	void addFansPage(char* pageName);
	void printMembers() const;
	void printFansPages()const;
	void connectBetween2Users(Member* user1, Member* user2);
	void connectBetweenUserToFanPage(Member* user1,  FanPage* fanPage1);
	void addStatusToUser(Users* u1,  Status* status);
	void printStatusesOfSpecificUser(Users* user)const;
	int getNumOfUsers() const { return numOfUseres; }
	Users* getUser(char* nameToSearch) const;
};


#endif // !_FACEBOOK

