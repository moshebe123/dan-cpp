#ifndef USERS_H
#define USERS_H

#include "Utils.h"

class Member;
#include "Statuses.h"


class Users
{
protected:
	char* name;
	Status** posts;
	Member** members;
	int numOfFriends, numOfFriendsPsize;
	int numOfStatuses, statusesPSize;
	Users() = default;
	Users(const Users& other) = delete;

public:
	Users(char* userName);
	virtual ~Users();
	char* getName();
	void printUser() const;
	void printMembers() const;
	void printStatuses() const;
	void addFriend(Member* friendToAdd) ;
	void addStatus(Status* statusToAdd);
	bool operator>(const Users& other) const;



};

#endif // ! USERS_H
#pragma once
